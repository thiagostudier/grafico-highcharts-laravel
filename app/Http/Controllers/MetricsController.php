<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Metrics;

class MetricsController extends Controller
{
    //
    public function index(){
        
        $data = Metrics::getMetrics(); //PEGAR TODOS OS DADOS DO JSON

        $legends = Metrics::getMetricsLegends(); //PEGAR TODAS AS "ETIQUETAS/LEGENDAS" DO JSON 

        $values = Metrics::getMetricsValues(); //PEGAR APENAS OS DO JSON VALORES

        $items = []; //CRIAR NOVO ARRAY

        $dates = []; //CRIAR NOVO ARRAY APENAS COM AS DATAS PARA QUE VIREM DADOS NO EIXO X E NA TOOLTIP 

        foreach($legends as $key_legend => $legend){ //PERCORRER TODAS AS "ETIQUETAS/LEGENDAS" DO JSON
            
            $items[$key_legend]['legend'] = $legend; //PEGAR "ETIQUETA/LEGENDA" DO DADO E INSERIR NO ARRAY

            foreach($values as $key_value => $value){ //PERCORRER TODOS OS VALORES DO JSON
                
                // ADICIONAR DENTRO DO ARRAY EM CADA "ETIQUETA/LEGENDA" OS SEUS DADOS
                // POR EXEMPLO: 
                // "legend": "affiliates",
                // "values": {
                //     "2018-2-1": "949,84",
                //     "2018-2-2": "949,84",
                //     "2018-2-3": "949,84",
                // }

                $items[$key_legend]['values'][$key_value] = [ //ADICIONAR DENTRO DO ARRAY EM CADA "ETIQUETA/LEGENDA" OS SEUS DADOS
                    'name' => $value->name,
                    'value'=> $value->data->{$legend}
                ];

                if(!in_array($value->name, $dates)){ //ADICIONAR AS DADAS EM UM ARRAY
                    $dates[] = $value->name;
                }
            }

        }

        return view('index', compact('data', 'items', 'dates'));
    }
}
