<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metrics extends Model
{
    //PEGAR METRICS
    public static function getMetrics(){
        $url = "get_metrics.json";
        $data = json_decode(file_get_contents($url));
        return $data;
    }

    //PEGAR METRICS LEGENDS
    public static function getMetricsLegends(){
        $url = "get_metrics.json";
        $data = json_decode(file_get_contents($url));

        $items = [];

        foreach($data->metrics->revenue_by_medium->data->series[0]->data as $key => $item){
            $items[] = $key;
        }
        return $items;
    }

    //PEGAR METRICS VALUES
    public static function getMetricsValues(){
        $url = "get_metrics.json";
        $data = json_decode(file_get_contents($url));

        $values = $data->metrics->revenue_by_medium->data->series;
        
        return $values;
    }
    //PEGAR AS CORES DA PALETA DE CORES DO PROJETO
    public static function getMetricsColors($id){
        $colors = ['#00cbff', '#1d6bc4', '#6638a2', '#ff0087', '#ffcb64', '#ff5100', '#dddddd']; //TODAS AS CORES
        if(isset($colors[$id])){ //PEGAR A COR PELO NÚMERO DO "FOREACH"
            return $colors[$id];
        }else{ 
            //SE O NÚMERO DO "FOREACH" FOR MAIOR DO QUE O NÚMEOR DE ITENS NO ARRAY, É RODADO O CÓDIGPO WHILE
            while($id > (count($colors)-1)){
                $id = $id - ( count($colors) );
            };
            return $colors[ $id ];

        }
    }
    
}
