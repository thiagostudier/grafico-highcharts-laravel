<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Gráfico PMWEB</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('highcharts/highcharts.js') }}" defer></script>
        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('highcharts/css/highcharts.css') }}" rel="stylesheet">
    </head>
    <body class="back-white">
        <div class="container">
            <div class="row mb-2">
                <div class="col pt-3 pb-3">
                    <div class="title">
                        <h3 class="d-inline-block text-uppercase"><b>{{$data->metrics->revenue_by_medium->title}}</b></h3>
                        <h5 class="d-inline-block color-gray-light">{{\Carbon\Carbon::parse($data->start_date)->format('d/m/y')}} - {{\Carbon\Carbon::parse($data->end_date)->format('d/m/y')}}</h5>
                    </div>
                    <h6 class="color-gray-light">{{$data->metrics->revenue_by_medium->description}}</h6>
                </div>
            </div>
            <div class="row mb-2">            
                <div class="col" id="chart-metric"></div>
            </div>
            <div class="row mb-2">
                <div class="col">
                    <h4 class="d-inline-block text-uppercase"><b>Orientações para o Back-End</b></h4>
                    <p>Para que a leitura do JSON seja mais eficaz e se adeque ao Highcharts, é melhor que a organização do mesmo seja (por exemplo): </p>
                    <p>
                        "affiliates": { <br />
                        <span style="margin-left: 20px;"></span>"type": "float", <br />
                        <span style="margin-left: 20px;"></span>"value": "null", <br />
                        <span style="margin-left: 20px;"></span>"data": { <br />
                            <span style="margin-left: 40px;"></span> "2018-2-1": "949,84", <br />
                            <span style="margin-left: 40px;"></span>"2018-2-2": "623,26", <br />
                            <span style="margin-left: 40px;"></span>"2018-2-3": "746,40", <br />
                            <span style="margin-left: 40px;"></span>... <br />
                            <span style="margin-left: 20px;"></span>} <br />
                        }
                    </p>
                    <p>Trocar os dados dos valores de vírgulas (,) para pontos (.), facilitando a conversão do valor:</p>
                    <p>"2018-2-1": 949.84</p>
                    
                </div>
            </div>
        </div>
    </body>

    <script>

        //FUNÇÃO PARA CONVERTER VALOR PARA REAL R$
        function formatMoney(n, c, d, t) {
            c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        }
        
        // ARRAY DOS DIAS PARA O EIXO X
        var day = [
            <?php foreach($dates as $date){ ?>
                '<?=\Carbon\Carbon::parse($date)->format('d')?>',
            <?php } ?>
        ]; 

        //ARRAY DOS DIAS PARA TOOLTIP
        var date_array = [
            <?php foreach($dates as $date){ ?>
                '<?=\Carbon\Carbon::parse($date)->format('d/m/Y')?>',
            <?php } ?>
        ]; 

        document.addEventListener('DOMContentLoaded', function () {

            var myChart = Highcharts.chart('chart-metric', {

                title: {
                    text: null
                },

                subtitle: {
                    text: null
                },

                yAxis: {
                    title: {
                        text: null
                    }
                },

                legend: {
                    layout: 'horizontal',
                    align: 'right',
                    verticalAlign: 'top',
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                    }
                },

                tooltip: {
                    formatter: function() {
                        var tooltip = date_array[this.x];
                        var data = this.series.chart.series[1].data;
                        var index = this.point.index;
                        tooltip += '<br><span style="color:' + this.series.color + '">' + this.series.name + '</span>: R$' + formatMoney(this.y);
                        return tooltip;
                    } 
                },
                
                xAxis: {
                    tickInterval: 1,
                    labels: {
                        enabled: true,
                        formatter: function() { 
                            return day[this.value];
                        },
                    }
                },

                series: [
                    //PERCORRER ARRAY LEGENDAS
                    <?php foreach($items as $key => $item){ ?>
                    {
                        name: '<?=$item['legend']?>',
                        data: [
                            // PERCORRER ARRAY DOS VALORES DE CADA LEGENDA
                            <?php foreach($item['values'] as $item){ ?>
                                <?=str_replace(",", ".", $item['value'])?>,
                            <?php } ?>
                        ],
                        //PEGAR COR DA PALETA
                        color: '<?=\App\Metrics::getMetricsColors($key)?>'
                    },
                    <?php } ?> 
                ],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }

            });
        });
    
    </script>

</html>
